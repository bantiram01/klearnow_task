import * as TYPE from "./types";

//Action creators For Reducers
export const showAlert = data => ({
	type: TYPE.HANDLE_ERROR,
	data: { showAlert: true, ...data }
});
export const hideLoader = () => ({ type: TYPE.HIDE_LOADER });
export const showLoader = () => ({ type: TYPE.SHOW_LOADER });
