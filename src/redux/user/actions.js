import RestClient from "../../utilities/RestClient";
import * as TYPE from "./types";
import Messages from "../../utilities/messages";
import * as appActions from "../app/actions";

//Action Creator For Reducers
export const recentlySearched = data => ({
  type: TYPE.RECENTLY_SEARCHED,
  data
});

export const saveToMyContent = data => ({
  type: TYPE.SAVE_TO_MYCONTENT,
  data
});

export const rateContent = data => ({
  type: TYPE.CUSTOM_RATE,
  data
});

export const removeContent = data => ({
  type: TYPE.REMOVE_CONTENT,
  data
});

export const markAsWatched = data => ({
  type: TYPE.MARK_AS_WATCHED,
  data
});

export const searchForContent = (params, cb) => {
  return dispatch => {
    dispatch(appActions.showLoader());

    RestClient.get(params)
      .then(result => {
        result.searchText = params.s;
        dispatch(recentlySearched(result));
        dispatch(appActions.hideLoader());
      })
      .catch(e => {
        console.log(e, "eee");
      });
  };
};

export const contentDetails = (params, cb) => {
  return dispatch => {
    dispatch(appActions.showLoader());
    RestClient.get(params)
      .then(result => {
        cb(result);
        dispatch(appActions.hideLoader());
      })
      .catch(e => {
        console.log(e, "eee");
      });
  };
};

export const seasonOrEpisodeDetails = (params, cb) => {
  return dispatch => {
    dispatch(appActions.showLoader());
    RestClient.get(params)
      .then(result => {
        cb(result);
        dispatch(appActions.hideLoader());
      })
      .catch(e => {
        console.log(e, "eee");
      });
  };
};
