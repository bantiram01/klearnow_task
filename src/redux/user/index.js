import * as TYPE from "./types";

// initial state of the reducer
const initialState = {
	searchResults: [],
	myContent: []
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case TYPE.RECENTLY_SEARCHED:
			return { ...state, searchResults: action.data };

		case TYPE.SAVE_TO_MYCONTENT:
			action.data.customRating = 0;
			action.data.watched = false;

			state.myContent.push(action.data);
			return {
				...state
			};

		case TYPE.CUSTOM_RATE:
			var { rating, id } = action.data,
				index = state.myContent.findIndex(data => data.imdbID === id);
			state.myContent[index].customRating = rating;
			return { ...state };

		case TYPE.REMOVE_CONTENT:
			var { id } = action.data,
				index = state.myContent.findIndex(data => data.imdbID === id);
			state.myContent.splice(index, 1);

			return { ...state };

		case TYPE.MARK_AS_WATCHED:
			var { id } = action.data,
				index = state.myContent.findIndex(data => data.imdbID === id);
			state.myContent[index].watched = true;

			return { ...state };
		default:
			return state;
	}
}
