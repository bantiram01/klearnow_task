// Actions For User Account
export const RECENTLY_SEARCHED = "RECENTLY_SEARCHED";
export const SAVE_TO_MYCONTENT = "SAVE_TO_MYCONTENT";
export const CUSTOM_RATE = "CUSTOM_RATE";
export const REMOVE_CONTENT = "REMOVE_CONTENT";
export const MARK_AS_WATCHED = "MARK_AS_WATCHED";
