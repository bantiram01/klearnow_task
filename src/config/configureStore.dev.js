import { applyMiddleware, createStore } from "redux";
import { persistStore } from "redux-persist";
import { composeWithDevTools } from "redux-devtools-extension";
import * as thunk from "redux-thunk";
import reducer from "../redux";
import { routerMiddleware } from "react-router-redux";

// configured store under a development mode
export default function configureStore(history) {
	const store = createStore(
		reducer,
		composeWithDevTools(
			applyMiddleware(thunk.default, routerMiddleware(history))
		)
	);

	const persistor = persistStore(store);

	return { persistor, store };
}
