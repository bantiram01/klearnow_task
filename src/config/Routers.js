import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import AppRoute from "./AppRoute";
import NotFound from "../components/NotFound";
import Home from "../containers/landingPage";
import MyContent from "../containers/myContent";
import DetailsPage from "../containers/details";
import SeasonDetails from "../containers/seasonDetails";
import { frontLayout, dashboardLayout } from "../components/Layouts";

// application router goes here...
const Routers = ({ store, history }) => {
	return (
		<BrowserRouter>
			<Switch>
				<AppRoute
					exact={true}
					path="/"
					component={Home}
					layout={frontLayout}
				/>
				<AppRoute
					path="/my-content"
					component={MyContent}
					layout={frontLayout}
				/>
				<AppRoute
					path="/details/:type/:id"
					component={DetailsPage}
					layout={frontLayout}
				/>
				<AppRoute
					path="/season-details/:id"
					component={SeasonDetails}
					layout={frontLayout}
				/>
				<AppRoute path="*" component={NotFound} layout={frontLayout} />
			</Switch>
		</BrowserRouter>
	);
};
export default Routers;
