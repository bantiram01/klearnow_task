import React from "react";
import { Route } from "react-router-dom";

const AppRoute = ({
  component: Component,
  layout: Layout,
  requireAuth: requireAuth,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      requireAuth && requireAuth();
      return (
        <Layout>
          <Component {...props} />
        </Layout>
      );
    }}
  />
);

export default AppRoute;
