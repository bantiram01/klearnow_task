import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardSubtitle,
  CardFooter,
  Button,
  CardTitle,
  CardText,
  CardImg,
  Alert,
  CardHeader,
  Container,
  Row,
  Col,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import { swal } from "react-redux-sweetalert2";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  seasonOrEpisodeDetails,
  saveToMyContent
} from "../../redux/user/actions";
import Messages from "../../utilities/messages";

const SeasonDetails = props => {
  let { id } = useParams();
  const [season, setSeason] = useState(1);
  const [seasonDet, setSeasonDetails] = useState({});
  const [episodeDet, setEpisodeDetails] = useState({});

  const [seasonDropdownOpen, setSeasonDropdownOpen] = useState(false);

  const seasonToggle = () => setSeasonDropdownOpen(prevState => !prevState);

  const [episode, setEpisode] = useState(1);
  const [episodeDropdownOpen, setEpisodeDropdownOpen] = useState(false);

  const episodeToggle = () => setEpisodeDropdownOpen(prevState => !prevState);

  // api call for getting a season details i,e season + episodes
  useEffect(() => {
    let detailsObj = {
      i: id,
      Season: season
    };
    props.seasonOrEpisodeDetails(detailsObj, res => {
      setSeasonDetails(res);
    });
  }, [season]);

  // api call when any of the season or episode is changed in dropdown
  useEffect(() => {
    let episodeDetailsObj = {
      i: id,
      Season: season,
      Episode: episode,
      plot: "full"
    };
    props.seasonOrEpisodeDetails(episodeDetailsObj, res => {
      setEpisodeDetails(res);
    });
  }, [season, episode]);

  // content is saved under my content section
  const handleSaveBtn = () => {
    props.dispatch(saveToMyContent(episodeDet));
    props.swal({
      title: "Success.",
      text: Messages.successfullySaved,
      type: "success"
    });
  };

  let { totalSeasons, Episodes } = seasonDet,
    {
      Title,
      Director,
      Year,
      Released,
      Season,
      Episode,
      Poster,
      Plot,
      Type,
      imdbID
    } = episodeDet,
    { myContent } = props.user,
    alreadyExists = myContent.find(e => e.seriesID == id && e.imdbID == imdbID);

  return (
    <Container fluid>
      <Row style={{ marginTop: 50 }}>
        <Col md="3" />

        <Col md="6">
          <Row>
            <Col>
              <Alert color="primary">
                SEASON NAME: {seasonDet.Title && seasonDet.Title.toUpperCase()}
              </Alert>
            </Col>
          </Row>
          <Row>
            <Col md="6">
              <Dropdown isOpen={seasonDropdownOpen} toggle={seasonToggle}>
                Choose a season:{" "}
                <DropdownToggle color="link" caret>
                  {season}
                </DropdownToggle>
                <DropdownMenu>
                  {Array(totalSeasons && Number(totalSeasons))
                    .fill(0)
                    .map((data, index) => {
                      return (
                        <DropdownItem
                          onClick={() => {
                            setSeason(index + 1);
                            setEpisode(1);
                          }}
                        >
                          {index + 1}
                        </DropdownItem>
                      );
                    })}
                </DropdownMenu>
              </Dropdown>
            </Col>
            <Col md="6">
              <Dropdown isOpen={episodeDropdownOpen} toggle={episodeToggle}>
                Choose an episode:{" "}
                <DropdownToggle color="link" caret>
                  {episode}
                </DropdownToggle>
                <DropdownMenu>
                  {Array(Episodes && Episodes.length)
                    .fill(0)
                    .map((data, index) => {
                      return (
                        <DropdownItem
                          onClick={() => {
                            setEpisode(index + 1);
                          }}
                        >
                          {index + 1}
                        </DropdownItem>
                      );
                    })}
                </DropdownMenu>
              </Dropdown>
            </Col>
          </Row>
          <Card style={{ marginTop: "2%" }}>
            <CardHeader>
              <CardTitle>
                <h4>{Title}</h4>
              </CardTitle>
            </CardHeader>
            <CardImg top width="100%" src={Poster} alt="Card image cap" />
            <CardBody>
              <CardSubtitle>
                <h6>
                  Season: {Season}, Episode: {Episode}{" "}
                </h6>
              </CardSubtitle>
              <CardText>{Plot}</CardText>
              <CardText>
                <small className="text-muted">Release date: {Released}</small>
              </CardText>
            </CardBody>
            <CardFooter>
              <Button
                color="primary"
                block
                onClick={handleSaveBtn}
                disabled={alreadyExists}
              >
                {alreadyExists ? Messages.alreadySaved : "Save "}{" "}
                <i class="fa fa-save"></i>
              </Button>
            </CardFooter>
          </Card>
        </Col>
        <Col md="3" />
      </Row>
    </Container>
  );
};

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => ({
  dispatch,
  seasonOrEpisodeDetails: bindActionCreators(seasonOrEpisodeDetails, dispatch),
  swal: bindActionCreators(swal.showAlert, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SeasonDetails);
