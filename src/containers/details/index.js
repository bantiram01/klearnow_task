import React, { useState, useEffect } from "react";
import {
	Card,
	CardBody,
	CardSubtitle,
	CardFooter,
	Button,
	CardTitle,
	CardText,
	CardImg,
	CardHeader,
	Container,
	Row,
	Col
} from "reactstrap";
import { Link, useParams } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { swal } from "react-redux-sweetalert2";
import { contentDetails, saveToMyContent } from "../../redux/user/actions";
import Messages from "../../utilities/messages";

const DetailsPage = props => {
	let { id, type } = useParams();
	const [details, setDetails] = useState({});

	// api call for getting the content details on page load
	useEffect(() => {
		let detailsObj = {
			i: id,
			type: type,
			plot: "full"
		};
		props.contentDetails(detailsObj, res => {
			setDetails(res);
		});
	}, [id]);

	// content is saved under my content section on clicking save button
	const handleSaveBtn = () => {
		props.dispatch(saveToMyContent(details));
		props.swal({
			title: "Success.",
			text: Messages.successfullySaved,
			type: "success"
		});
	};

	let {
			Title,
			Year,
			Director,
			Plot,
			Released,
			Type,
			Poster,
			Production,
			BoxOffice,
			Awards,
			Actors,
			Genre
		} = details,
		{ myContent } = props.user,
		alreadyExists = myContent.find(e => e.imdbID == id);

	return (
		<Container fluid>
			{Title && (
				<Row style={{ marginTop: 50 }}>
					<Col md="3" />

					<Col md="6">
						<Card>
							<CardHeader>
								<CardTitle>
									<h4>
										{Title} ({Year}) | {Type && Type.toUpperCase()}
									</h4>
								</CardTitle>
							</CardHeader>

							<CardBody>
								<CardSubtitle>
									<h6>
										Directed by: {Director} ({Genre})
									</h6>
								</CardSubtitle>

								<CardText>
									<small className="text-muted">Release date: {Released}</small>
								</CardText>
							</CardBody>
							<CardBody>
								<CardText>{Plot}</CardText>
								<CardText>
									<small className="text-muted">
										Produced by: {Production}, earned {BoxOffice} on the box
										office.
									</small>
								</CardText>
							</CardBody>
							<CardBody>
								<CardSubtitle>
									<h6>{Awards}</h6>
								</CardSubtitle>
								<CardSubtitle>
									<h6>{Actors}</h6>
								</CardSubtitle>
							</CardBody>
							<CardFooter>
								{type == "series" ? (
									<Link to={`/season-details/${id}`}>
										<Button color="primary" block>
											Explore <i class="fa fa-search"></i>
										</Button>
									</Link>
								) : (
									<Button
										color="primary"
										block
										disabled={alreadyExists}
										onClick={handleSaveBtn}
									>
										{alreadyExists ? Messages.alreadySaved : "Save "}
										<i class="fa fa-save"></i>
									</Button>
								)}
							</CardFooter>
						</Card>
					</Col>
					<Col md="3" />
				</Row>
			)}
		</Container>
	);
};

const mapStateToProps = state => {
	return {
		user: state.user
	};
};

const mapDispatchToProps = dispatch => ({
	dispatch,
	contentDetails: bindActionCreators(contentDetails, dispatch),
	swal: bindActionCreators(swal.showAlert, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPage);
