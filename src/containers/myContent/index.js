import React, { useState } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Container
} from "reactstrap";
import classnames from "classnames";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { swal } from "react-redux-sweetalert2";
import SavedContent from "./savedContent";
import WatchedContent from "./watchedContent";
import {
  rateContent,
  removeContent,
  markAsWatched
} from "../../redux/user/actions";
import Messages from "../../utilities/messages";

const MyContent = props => {
  const [activeTab, setActiveTab] = useState("1");

  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  // handle star rating on click of a rating section
  const handleRatingChange = (rating, id) => {
    let ratingObj = {
      rating,
      id
    };
    props.dispatch(rateContent(ratingObj));
    props.swal({
      title: "Success.",
      text: Messages.ratingUpdated,
      type: "success"
    });
  };

  // content removed from my content section
  const handleRemoveContent = id => {
    let removeObj = {
      id
    };
    props.dispatch(removeContent(removeObj));
    props.swal({
      title: "Success.",
      text: Messages.removedSuccessfully,
      type: "success"
    });
  };

  // content is marked as watched and moved to the watched section
  const handleMarkAsWatched = id => {
    let markAsWatchedObj = {
      id
    };
    props.dispatch(markAsWatched(markAsWatchedObj));
    props.swal({
      title: "Success.",
      text: Messages.successfullyMarked,
      type: "success"
    });
  };

  let { myContent } = props.user,
    savedContent = myContent.filter(e => !e.watched),
    watchedContent = myContent.filter(e => e.watched);
  return (
    <Container fluid>
      <div style={{ marginTop: "3%" }}>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === "1" })}
              onClick={() => {
                toggle("1");
              }}
            >
              My Content
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === "2" })}
              onClick={() => {
                toggle("2");
              }}
            >
              Watched
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <SavedContent
              savedContent={savedContent}
              handleRatingChange={handleRatingChange}
              removeContent={handleRemoveContent}
              markAsWatched={handleMarkAsWatched}
            />
          </TabPane>
          <TabPane tabId="2">
            <WatchedContent
              watchedContent={watchedContent}
              handleRatingChange={handleRatingChange}
              removeContent={handleRemoveContent}
              markAsWatched={handleMarkAsWatched}
            />
          </TabPane>
        </TabContent>
      </div>
    </Container>
  );
};

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => ({
  dispatch,
  swal: bindActionCreators(swal.showAlert, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(MyContent);
