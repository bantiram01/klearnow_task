import React, { useState } from "react";
import {
  Card,
  Button,
  CardTitle,
  CardHeader,
  CardBody,
  CardFooter,
  CardSubtitle,
  CardText,
  Row,
  Col
} from "reactstrap";
import StarRatings from "react-star-ratings";

const SavedContent = props => {
  let { savedContent } = props;

  return (
    <Row>
      {savedContent.map((item, index) => {
        return (
          <Col md="12">
            <Card style={{ marginTop: 10 }}>
              <CardHeader>
                <CardTitle>
                  <h4>
                    {item.Title} ({item.Year})
                  </h4>
                </CardTitle>
              </CardHeader>

              <CardBody>
                <CardSubtitle>
                  <h6>Directed by: {item.Director}</h6>
                </CardSubtitle>
                <CardText>{item.Plot}</CardText>
                <CardText>
                  <small className="text-muted">
                    Release date: {item.Released}
                  </small>
                </CardText>
                <StarRatings
                  rating={item.customRating}
                  starRatedColor="blue"
                  changeRating={rating =>
                    props.handleRatingChange(rating, item.imdbID)
                  }
                  numberOfStars={10}
                  name="rating"
                  starDimension="25px"
                  starSpacing="10px"
                />
              </CardBody>
              <CardFooter>
                <Button
                  size="sm"
                  color="success"
                  onClick={() => {
                    props.markAsWatched(item.imdbID);
                  }}
                >
                  Mark as watched <i class="fa fa-check"></i>
                </Button>{" "}
                <Button
                  size="sm"
                  color="danger"
                  onClick={() => {
                    props.removeContent(item.imdbID);
                  }}
                >
                  Remove <i class="fa fa-trash"></i>
                </Button>
              </CardFooter>
            </Card>
          </Col>
        );
      })}
    </Row>
  );
};

export default SavedContent;
