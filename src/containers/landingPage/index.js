import React, { useState } from "react";
import { Container, Row, Col, Button, Input, InputGroup } from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { searchForContent, saveToMyContent } from "../../redux/user/actions";
import SearchedContent from "./searchedContent";
import Messages from "../../utilities/messages";

const LandingPage = props => {
	let { searchResults } = props.user;
	const [searchText, handleSearchText] = useState("");

	// search for digital content on pressing search button
	const searchDigitalContent = () => {
		let searchObj = {
			s: searchText
		};
		props.searchForContent(searchObj);
	};

	// fetch more results on pressing a page in pagination bar
	const fetchPageResults = page => {
		let searchObj = {
			s: searchResults.searchText,
			page
		};
		props.searchForContent(searchObj);
	};

	return (
		<Container fluid>
			<Row>
				<Col style={{ marginTop: "5%" }}>
					<InputGroup>
						<Input
							placeholder={Messages.searchPlaceholder}
							onChange={e => {
								handleSearchText(e.target.value);
							}}
						/>
					</InputGroup>
				</Col>
			</Row>

			<Row style={{ marginTop: "2%" }}>
				<Col style={{ textAlign: "center" }}>
					<Button color="info" onClick={searchDigitalContent}>
						{Messages.searchBtnText}
					</Button>
				</Col>
			</Row>

			<SearchedContent
				searchResults={searchResults}
				fetchPageResults={fetchPageResults}
			/>
		</Container>
	);
};

const mapStateToProps = state => {
	return {
		user: state.user
	};
};

const mapDispatchToProps = dispatch => ({
	dispatch,
	searchForContent: bindActionCreators(searchForContent, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);
