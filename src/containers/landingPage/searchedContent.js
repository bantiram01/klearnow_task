import React, { useState } from "react";
import { Row, Col, Media } from "reactstrap";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";

const SearchedContent = props => {
	let { Search, totalResults } = props.searchResults,
		paginationPages = Number((totalResults / 10).toFixed());

	return (
		<div>
			<Row>
				{Search &&
					Search.length > 0 &&
					Search.map((item, index) => {
						return (
							<Col
								md="6"
								key={index}
								style={{
									marginTop: 30
								}}
							>
								<Media>
									<Media left middle>
										<img
											src={
												item.Poster != "N/A"
													? item.Poster
													: "https://i7.pngguru.com/preview/53/416/43/film-frame-photography-screenwriter-title-frame.jpg"
											}
											className="mediaLeftImg"
										/>
									</Media>
									<Media body style={{ marginLeft: 10 }}>
										<Media heading>
											<Link
												to={`/details/${item.Type}/${item.imdbID}`}
											>
												<h6>
													{item.Title} - ({item.Year})
												</h6>
											</Link>
										</Media>
										<p>{item.Type}</p>
									</Media>
								</Media>
							</Col>
						);
					})}
			</Row>
			{totalResults && totalResults > 10 && (
				<Row style={{ marginTop: "3%" }}>
					<Col className="alignItemsCenter">
						<ReactPaginate
							previousLabel={"Previous"}
							nextLabel={"Next"}
							breakLabel={"..."}
							pageCount={paginationPages}
							marginPagesDisplayed={2}
							pageRangeDisplayed={5}
							onPageChange={({ selected }) =>
								props.fetchPageResults(selected + 1)
							}
							subContainerClassName={"pages pagination"}
							breakClassName={"page-item"}
							breakLinkClassName={"page-link"}
							containerClassName={"pagination"}
							pageClassName={"page-item"}
							pageLinkClassName={"page-link"}
							previousClassName={"page-item"}
							previousLinkClassName={"page-link"}
							nextClassName={"page-item"}
							nextLinkClassName={"page-link"}
							activeClassName={"active"}
						/>
					</Col>
				</Row>
			)}
		</div>
	);
};

export default SearchedContent;
