import axios from "axios";

const baseUrl = "http://www.omdbapi.com/?apikey=9b4c2567&";

class RestClient {
  // get method
  static get(params) {
    var queryString = Object.keys(params)
      .map(key => key + "=" + params[key])
      .join("&");

    return new Promise(function(resolve, reject) {
      axios
        .get(`${baseUrl}${queryString}`)
        .then(function(response) {
          resolve(response.data);
        })
        .catch(function(error) {
          reject(error);
        })
        .then(function() {
          // always executed
        });
    });
  }

  /***************
    ......
    ......
    ......
    ....other api methods like post, delete, put goes here.....
    ......
    ......
    ......

  ************/
}

export default RestClient;
