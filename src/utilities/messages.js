const Message = {
	error: "Error message here.",
	succes: "Success message here.",
	successfullySaved: "successfully saved to my content section..",
	alreadySaved: "already saved under my content section ",
	searchPlaceholder: "search for movies, series or episodes",
	searchBtnText: "search your favorite digital content :)",
	ratingUpdated: "content ratings successfully updated..",
	removedSuccessfully: "content successfully removed..",
	successfullyMarked: "content sucessfully marked as watched.."
};

module.exports = Message;
